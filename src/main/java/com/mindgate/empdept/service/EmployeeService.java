package com.mindgate.empdept.service;

import java.util.List;

import com.mindgate.empdept.domain.Employee;

public interface EmployeeService {
	public void addEmployee(Employee employee);
	public Employee getEmployeeById(int empId);
	public List<Employee> getEmployees();
}