package com.mindgate.empdept.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.mindgate.empdept.domain.Employee;
import com.mindgate.empdept.repository.EmployeeRepository;

	@Service
	public class EmployeeServiceImpl implements EmployeeService {

		@Autowired
		private EmployeeRepository employeerepository;	

		public EmployeeRepository getEmployeerepository() {
			return employeerepository;
		}

		public void setEmployeerepository(EmployeeRepository employeerepository) {
			this.employeerepository = employeerepository;
		}
		
		public void addEmployee(Employee employee) throws DataAccessException {
			employeerepository.addEmployee(employee);		
		}
		
		public Employee getEmployeeById(int empId) throws DataAccessException {
			return employeerepository.getEmployeeById(empId);		
		}
		
		public List<Employee> getEmployees() throws DataAccessException {
			return employeerepository.getEmployees();		
		}
		

	}


