package com.mindgate.empdept.repository;

import java.util.List;

import com.mindgate.empdept.domain.Employee;



public interface EmployeeRepository {
	public void addEmployee(Employee employee);
	public Employee getEmployeeById(int empId);
	public List<Employee> getEmployees();
}