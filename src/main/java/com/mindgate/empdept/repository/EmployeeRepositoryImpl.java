package com.mindgate.empdept.repository;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.stereotype.Repository;
 
 

import com.mindgate.empdept.domain.Employee;

@Repository
	public class EmployeeRepositoryImpl implements EmployeeRepository{
		
		private static String INSERT_EMPLOYEE_SQL = "insert into employees(emp_name, salary,designation,age,dept_id) values(?,?,?,?)";
		private static String SELECT_ALL_EMPLOYEES_SQL = "select * from employees";
		private static String SELECT_EMPLOYEE_SQL = "select * from employees where emp_id = ?";
		
		private JdbcTemplate jdbcTemplate;	

		@Autowired
		public void setDataSource(DataSource ds) {
		   	jdbcTemplate = new JdbcTemplate(ds);
		} 
		
		public void addEmployee(Employee employee) throws DataAccessException {
			//JdbcTemplate.update(INSERT_EMPLOYEE_SQL, employee.getEmpName(),employee.getEmpSalary(),employee.getDesignation(),employee.getAge(), employee.getDeptId());		
		}
		
		public Employee getEmployeeById(int empId) throws DataAccessException {
			return jdbcTemplate.queryForObject(SELECT_EMPLOYEE_SQL, new Object[]{empId}, new EmployeeRowMapper());
		}
			
		public List<Employee> getEmployees() throws DataAccessException {
			return this.jdbcTemplate.query(SELECT_ALL_EMPLOYEES_SQL, 
					new EmployeeRowMapper());
		}
		
		private static final class EmployeeRowMapper implements RowMapper<Employee>{
			
			public Employee mapRow(ResultSet rs, int rownum) throws SQLException {
				Employee  employee = new  Employee();
				employee.setEmpId(rs.getInt("emp_id"));
				employee.setEmpName(rs.getString("emp_name"));
				employee.setEmpSalary(rs.getInt("emp_salary"));
				employee.setDesignation(rs.getString("emp_designation"));
				return employee;
			}

		}
		

	}


