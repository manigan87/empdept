<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<h2>Employees List</h2>
		<c:if test="${employee != null}">  
		<table border="2" width="70%" cellpadding="2">  
			<tr><th>Id</th><th>Name</th><th>Salary</th><th>Designation</th></tr>  
   			<c:forEach var="employee" items="${employees}">   
   				<tr>  
   					<td>${employee.empId}</td>  
   					<td>${employee.empName}</td>  
   					<td>${employee.empSalary}</td>  
   					<td>${employee.empDesignation}</td>  
   				</tr>  
   			</c:forEach>  
   		</table>
   		</c:if>
   		<c:if test="{message != null}">
   		<p>${message}<p>
   		</c:if>  
	</body>
</html>