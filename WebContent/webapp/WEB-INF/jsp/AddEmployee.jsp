<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Add New Employee</h2>
      <form:form method = "POST" action = "/employee/addEmployee">
         <table>
            <tr>
               <td><form:label path = "empId">Employee Id</form:label></td>
               <td><form:input path = "empId" /></td>
            </tr>
            <tr>
               <td><form:label path = "empName">Employee Name</form:label></td>
               <td><form:input path = "empName" /></td>
            </tr>
            <tr>
               <td><form:label path = "empSalary">Employee Salary</form:label></td>
               <td><form:input path = "empSalary" /></td>
            </tr>
            <tr>
               <td><form:label path = "empDesignation">Employee Designation</form:label></td>
               <td><form:input path = "empDesignation" /></td>
            </tr>
            <tr>
               <td colspan = "2"><input type = "submit" value = "Submit"/></td>
            </tr>
         </table>  
      </form:form>
      
      
   	  <c:if test="{message != null}">
   		<p>${message}<p>
   	  </c:if>
</body>
</html>