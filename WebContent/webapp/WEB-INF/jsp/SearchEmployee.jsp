<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<h2>Employee Search</h2>  
		<form:form method = "GET" action = "/employee/searchEmployee">
        	<form:label path = "empId">Employee Id</form:label>
        	<form:input path = "empId" />
        </form:form>
        <c:if test="${employee != null}">
		<table border="2" width="70%" cellpadding="2">  
			<tr><th>Id</th><th>Name</th><th>Salary</th><th>Designation</th></tr>  
   			<tr>  
   				<td>${employee.empId}</td>  
   				<td>${employee.empName}</td>  
   				<td>${employee.empSalary}</td>  
   				<td>${employee.empDesignation}</td>  
   			</tr>   			  
   		</table>
   		</c:if>
   		<c:if test="{message != null}">
   		<p>${message}<p>
   		</c:if>
   		  
	</body>
</html>